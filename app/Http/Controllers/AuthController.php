<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register (){
        return view('tugas.register');
    }
    public function signup(Request $request){
        $firstname = $request['namadepan'];
        $lastname = $request ['namabelakang'];
        return view('tugas.welcome',compact ('firstname','lastname'));
    }
}
