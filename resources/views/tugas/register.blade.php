<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="post"> 
        @csrf
        <label>First Name: </label> <br><br>
        <input type="text" name="namadepan" id=""> <br><br>
        <label>Last Name: </label> <br><br>
        <input type="text" name="namabelakang" id=""> <br><br>
        <label>Gender: </label> <br><br>
        <input type="radio" name="Gender" Value="Male"> Male <br>
        <input type="radio" name="Gender" Value="Female"> Female <br>
        <input type="radio" name="Gender" Value="Other"> Other <br>
        <label>Nationality:</label> <br><br>
        <select name="Nationality">
            <option value="1">Indonesia</option>
            <option value="2">Amerika</option>
            <option value="3">Inggris</option>
        </select> <br><br>
        <label>Language Spoken: </label> <br><br>
        <input type="checkbox" name="Language Spoken" Value="Bahasa Indonesia"> Bahasa Indonesia <br>
        <input type="checkbox" name="Language Spoken" Value="English"> English <br>
        <input type="checkbox" name="Language Spoken" Value="Other"> Other <br><br>
        <label>Bio: </label> <br><br>
        <textarea name="Bio" id="" cols="30" rows="10"></textarea> <br>

        <input type="submit" value="sign Up">

    </form>
</body>
</html>